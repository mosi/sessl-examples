#!/usr/bin/env bash

# This is a script for automatically running all experiments.
#
# It is triggered by the continuous integration system
# when a new SNAPSHOT version of SESSL is deployed.
#
# You will probably never want to execute this manually.

# be verbose
set -x

# fail on error
set -e

# fail errors in SESSL experiments
export SESSL_STOP_ON_ERROR="true"

# go into all subfolders and execute the contained experiment, delete the results afterwards
for dir in ./*/; do
  if [ -d "$dir" ]; then
    pushd $dir
    ../mvnw --batch-mode scala:script
    for resultdir in ./result*/; do
      if [ -d "$resultdir" ]; then
        rm -rf $resultdir
      fi
    done
    popd
  fi
done