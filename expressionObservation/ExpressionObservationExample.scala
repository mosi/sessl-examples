import sessl._
import sessl.mlrules._

execute {
  new Experiment with Observation with ParallelExecution with CSVOutput with ExpressionObservation {
    model = "./prey-predator.mlrj"
    simulator = SimpleSimulator()
    parallelThreads = -1

    stopTime = 100

    replications = 5

    scan("wolfGrowth" <~ (0.0001, 0.0002))

    observe("s" ~ count("Sheep"))
    observe("w" ~ count("Wolf"))
    observe("s_and_w" ~ Expr(V => V("s") + V("w")))
    observeAt(range(0, 1, 100))

    // only write this column to the result csv file
    csvColumns("s_and_w")

    withRunResult(writeCSV)
  }
}
