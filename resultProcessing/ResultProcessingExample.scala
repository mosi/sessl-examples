import sessl._
import sessl.mlrules._

execute {
  new Experiment with Observation {
    model = "./prey-predator.mlrj"
    simulator = SimpleSimulator()

    stopTime = 100

    observe("s" ~ count("Sheep"))
    observe("w" ~ count("Wolf"))
    observeAt(range(0, 1, 100))

    withRunResult { result =>
      val sheep = result.trajectory[Double]("s").toMap
      val wolves = result.trajectory[Double]("w").toMap

      // determine the time points with more wolfs than sheep
      val moreWolves = sheep.keys.filter(t => sheep(t) < wolves(t)).toSeq.sorted
      println(moreWolves.mkString(", "))
    }
  }
}
