import sessl._
import sessl.ml3._

execute {
  new Experiment with Observation {
    model = "./sir.ml3"

    stopCondition = Custom((state, time) => state.getAgentsAlive.isEmpty || time >= 100)

    // alternatively:
    // stopCondition = Custom((state, time) => state.getAgentsAlive.isEmpty) or AfterSimTime(100)

    set("a" <~ 0.03)
    set("b" <~ 0.05)

    val s = 950 * "new Person(status := \"susceptible\")"
    val i = 50 * "new Person(status := \"infected\")"
    initializeWith(s + ";" + i)

    observe("a" ~ agentCount("A"))
    observeAt(range(0, 1, 100))

    withExperimentResult(result =>
      println(result.mean[Int]("a"))
    )
  }
}
