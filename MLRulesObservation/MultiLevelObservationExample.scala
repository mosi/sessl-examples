import sessl._
import sessl.mlrules._

execute {
  new Experiment with Observation {
    model = "observationTestModel.mlrj"
    simulator = SimpleSimulator()
    stopTime = 100
    observeAt(0.0)

    observe("b4" ~ count("B(4.0)"))
    observe("b5" ~ count("B(5.0)"))
    observe("anyB" ~ count("*/B(_)"))
    observe("anyBInAnyA" ~ count("A(_)/B(_)"))
    observe("anyB2InAnyA" ~ count("A(_)/B(2.0)"))
    observe("anyB2InAnyAx" ~ count("A(x)/B(2.0)"))
  }
}
