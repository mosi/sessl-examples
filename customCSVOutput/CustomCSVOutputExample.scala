import java.io.{File, FileWriter}

import sessl._
import sessl.mlrules._

execute {
  new Experiment with Observation with ParallelExecution with CSVOutput {
    model = "./prey-predator.mlrj"
    simulator = SimpleSimulator()
    parallelThreads = -1

    stopTime = 100

    replications = 5

    scan("wolfGrowth" <~ (0.0001, 0.0002))

    val w = observe(count("Wolf"))
    observeAt(range(0, 1, 100))

    withReplicationsResult { r =>
      val configFile = new FileWriter(new File(s"config-${r.assignId}"))
      CSVConfigurationWriter.write(r.variableAssignment, configFile)

      val outputFile = new FileWriter(new File(s"output-${r.assignId}"))
      outputFile.write("maxWolves\n")
      for {
        rr <- r.runs
      } outputFile.write(s"${rr.values(w).max}\n")
      outputFile.close()
    }
  }
}
