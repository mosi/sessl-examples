import sessl._
import sessl.mlrules._

execute {
  new Experiment with Observation with ParallelExecution {
    model = "./simpleModel.mlrj"
    simulator = SimpleSimulator()
    parallelThreads = -1

    stopTime = 100

    batchSize = 3

    observe("a" ~ count("A"))
    observeAt(stopTime)

    replicationCondition = MeanConfidenceReached(
      confidence = 0.99,
      relativeHalfWidth = 0.01,
      observable = "a")

    withExperimentResult(result =>
      println(result.mean[Double]("a"))
    )
  }
}
